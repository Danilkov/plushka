import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/index';
import './styles.scss';

const ProductDescription = ({ src, title, description, list, price, listTitle }) => {
  return (
    <div className="container flex">
      <div className="product__media">{src && <img src={src} alt={title} />}</div>
      <div className="product__description">
        <div className="product__description-container">
          <h2 className="product__description-title">{title}</h2>
          {description && <p className="product__description-text">{description}</p>}
          {listTitle && (
            <ul className="product__description-list">
              {list.map(item => (
                <li key={item.listTitle}>{item.listTitle}</li>
              ))}
            </ul>
          )}
        </div>
        <div className="product__description-price">
          <span>Total:</span>
          <span>{price}$</span>
        </div>
      </div>
      <div className="product__buttons">
        <Button color={'black'}>add to basket</Button>
        <Button>buy now</Button>
      </div>
    </div>
  );
};

ProductDescription.defaultProps = {
  src: require('../../img/cat.jpg'),
};

ProductDescription.propTypes = {
  descriptionOfProduct: PropTypes.objectOf({
    src: PropTypes.string,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
      })
    ),
    price: PropTypes.number.isRequired,
  }),
};

export default ProductDescription;
