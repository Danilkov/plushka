import React from 'react';
import cn from 'classnames';
import './constants';
import './styles.scss';

const Button = ({ children, color, type, size }) => {
  return (
    <button
      className={cn('btn', {
        [`btn_color_${color}`]: !!color,
        [`btn_type_${type}`]: !!type,
        [`btn_width_${size}`]: !!size,
      })}
    >
      {children}
    </button>
  );
};

export default Button;
