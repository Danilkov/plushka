import React, { Component } from 'react';
import ProductDescription from '../ProductDescription/index';
import TitleOfBlock from '../TitleOfBlock/index';
import Card from '../Card/index';

import './styles.scss';

class ProductPage extends Component {
  render() {
    const { productDescription, productList } = this.props;
    return (
      <>
        <ProductDescription {...productDescription} />
        <TitleOfBlock />
        <div className="card-list">
          <div className="container container_flex">
            {productList.map(item => {
              return <Card {...item} />;
            })}
          </div>
        </div>
      </>
    );
  }
}

export default ProductPage;
