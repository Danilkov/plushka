import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';
import Button from '../Button/';

const Card = ({ src, title, name, price }) => {
  return (
    <div className="card">
      <div className="card__media">{src && <img src={src} alt={name} />}</div>
      <div className="card__description">
        <div className="card__description-name">{title}</div>
        <div className="card__description-price">{price} $</div>
        <Button>add to basket</Button>
      </div>
    </div>
  );
};

Card.propTypes = {
  props: PropTypes.objectOf({
    src: PropTypes.string,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};

export default Card;
