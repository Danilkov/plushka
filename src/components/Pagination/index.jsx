import React from 'react';
import './styles.scss';

const Pagination = () => {
  const pageNumbers = [1, 2, 3];

  console.log(pageNumbers);
  return (
    <ul className="pagination">
      {pageNumbers.map(item => (
        <li key={item} className="pagination__item">
          {item}
        </li>
      ))}
    </ul>
  );
};

// Pagination.propTypes = {
//   list: PropTypes.arrayOf(
//     PropTypes.shape({
//       title: PropTypes.string.isRequired,
//     })
//   ).isRequired,
// };

export default Pagination;
