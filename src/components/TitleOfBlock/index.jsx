import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const TitleOfBlock = ({ title }) => {
  return <>{title && <div className="title-of-block">{title}</div>}</>;
};

TitleOfBlock.propTypes = {
  titleOfBlock: PropTypes.objectOf({
    title: PropTypes.string,
  }),
};

export default TitleOfBlock;
