import React from 'react';
import PropTypes from 'prop-types';
import social from './constants';

import './styles.scss';

const SocialButtons = () => {
  return (
    <div className="socials">
      {social.map(item => (
        <a key={item.label} href="" className="socials-btn" aria-label={item.label}>
          <svg>
            <use xlinkHref={`${item.link}#${item.label}`} />
          </svg>
        </a>
      ))}
    </div>
  );
};

export default SocialButtons;
