import React from 'react';
import SocialButtons from './index';

export default {
  title: 'Social',
  component: SocialButtons,
};
const props = [
  {
    label: 'vk',
    link: require('../../img/socials-sprive.svg'),
  },
  {
    label: 'facebook',
    link: require('../../img/socials-sprive.svg'),
  },
  {
    label: 'ok',
    link: require('../../img/socials-sprive.svg'),
  },
  {
    label: 'youtube',
    link: require('../../img/socials-sprive.svg'),
  },
];

export const ButtonsSocial = () => <SocialButtons list={props} />;
