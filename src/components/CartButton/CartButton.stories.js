import React from 'react';
import CartOderButton from './index';

export default {
  title: 'Cart',
  component: CartOderButton,
};
const props = {
  title: 'Order',
  count: '23',
};

export const CartButton = () => {
  return (
    <div style={{ display: 'flex' }}>
      <CartOderButton {...props} />
    </div>
  );
};
