const welcomeData = {
  src: require('../../img/cat.jpg'),
  alt: 'cat',
  title: 'Your Best Value Proposition',
  subtitle: `Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed
                            do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
  text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
  author: 'Dhaka Oke, Product Designer',
};

export default welcomeData;
