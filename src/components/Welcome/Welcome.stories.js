import React from 'react';

import Welcome from './index';

export default {
  component: Welcome,
  title: 'Welcome',
};

const welcomeData = {
  link: require('../../img/cat.jpg'),
  alt: 'cat',
  title: 'Your Best Value Proposition',
  subtitle: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
  text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.`,
  author: 'Dhaka Oke, Product Designer',
};

export const Default = () => {
  return <Welcome {...welcomeData} />;
};
