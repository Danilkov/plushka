import React from 'react';
import CartHeader from '.';

export default {
  title: 'Cart',
  component: CartHeader,
};

const props = {
  title: 'Order',
  count: '23',
};

export const HeaderCart = () => <CartHeader {...props} />;
